import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { RestangularModule, Restangular } from 'ngx-restangular';
import { Http, Response } from '@angular/http';

import { ProcessHttpmsgService } from './process-httpmsg.service';

import { baseURL } from '../shared/baseurl';
import { Feedback } from '../shared/feedback';

import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class FeedbackService {

  constructor(private restangular: Restangular,
              private processHTTPMsgService: ProcessHttpmsgService) { }

  submitFeedback(feed: Feedback): Observable<Feedback[]> {
      console.log(feed);
      return this.restangular.all('feedback').post(feed);
  }
}
