export class Comment {
    rating: number;
    comment: string;
    author: string;
    date: string;
}

export const Ratings = [1, 2, 3, 4, 5];
